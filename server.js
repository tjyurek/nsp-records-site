import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import { isLoggedIn, getHomeDirectory } from './routes/util';
import registerRoutes from './routes/RouteRegistry';
import sendEmail from './routes/mail/email-client';

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
if (process.env.CLOG)
	app.use(morgan('dev'));

// Tell the server to look for static assets in ./client/dist
app.use(express.static(getHomeDirectory() + '/client/dist'));

// Enable CORS so that we can make HTTP request from webpack-dev-server
app.use((request, response, next) => {
	response.header("Access-Control-Allow-Origin", "*");
	response.header('Access-Control-Allow-Methods', 'GET');
	response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

	next();
});

registerRoutes(app);

app.route("*").get(isLoggedIn,(request, response) => {
	response.sendFile('client/dist/main.html', { root: getHomeDirectory() });
});

app.use((err, request, response, next) => {
	sendEmail(
		'joseph.s@nationalschoolproject.com',
		'Records Site Server Error',
		'/server-error.html',
		{
			timestamp: new Date().toString(),
			name: err.name,
			message: err.message,
			stack: err.stack,
		},
	)
	response.status(500);
	response.end();
});

export default app;
