import React from 'react';
import { Link } from 'react-router-dom';
import 'whatwg-fetch';
import SkyLight from 'react-skylight';
import _ from 'lodash';

import DatePicker from '../../elements/date-picker/DatePicker';

import DataCard from '../../elements/results-display/DataCard';
import DataBox from '../../elements/results-display/DataBox';
import MainGraph from '../../elements/graph/MainGraph';
import StoriesPictures from '../../elements/stories-display/StoriesPictures';
import Card from '../../elements/card/Card';

import styles from './main-component.css';

export default class MainComponent extends React.Component {
	constructor(props) {
		super(props);

		/*
		  Data Card Schema
		  ----------------
	
		  allTimeWitnessingDays:0,
		  allTimePeopleApproached:0,
		  allTimeGospelPresentations:0,
		  allTimeWitnessingIndicatedDecisions:0,
		  allTimeRallies:0,
		  allTimeRallyAttendance:0,
		  allTimeRallyIndicatedDecisions:0,
		  allTimeIndicatedDecisions:0,
		  allTimeSchoolsActive:0,
		  allTimeSchoolsCoached:0,
		*/

		this.state = {
			username: "",
			chapter: "",
			mySchools: [],

			dataCards: [],
			display: "databox",

			newRangeStart: "YYYY-MM-DD",
			newRangeEnd: "YYYY-MM-DD",

			search: "",
			chapters: [],
			filteredChapters: [],

			schools: [],
			filteredSchools: [],

			storiesRallies: [],
			storiesWitnessing: [],

			picturesRallies: [],
			picturesWitnessing: [],

			waitingForRange: false

		}

		this.modal = null;

		this.dataCardConfig = [
			{
				main: {
					display: "Total Rallies",
					data: "allTimeRallies"
				},
				children: [
					{
						display: "Total Attendance",
						data: "allTimeRallyAttendance"
					},
					{
						display: "Indicated Decisions",
						data: "allTimeRallyIndicatedDecisions"
					}
				]
			},
			{
				main: {
					display: "Total Wintessing Events",
					data: "allTimeWitnessingDays"
				},
				children: [
					{
						display: "People Approached",
						data: "allTimePeopleApproached"
					},
					{
						display: "Gospel Presentations",
						data: "allTimeGospelPresentations"
					},
					{
						display: "Indicated Decisions",
						data: "allTimeWitnessingIndicatedDecisions"
					},
				]
			},
			{
				main: {
					display: "Schools Coached",
					data: "allTimeSchoolsCoached"
				},
				children: [
					{
						display: "Schools Active",
						data: "allTimeSchoolsActive"
					}
				]
			}
		]

		this.dataCardFooter = {
			display: "Indicated Decisions",
			data: "allTimeIndicatedDecisions"
		}
	}

	fetchSchools() {
		fetch('/api/schools', { credentials: 'include' })
			.then(response => response.json())
			.then(
				schools => {
					this.setState({
						schools,
						filteredSchools: schools
					})
				}
			);
	}

	fetchChapters() {
		fetch('/api/chapters', { credentials: 'include' })
			.then(response => response.json())
			.then(
				chapters => {
					this.setState({
						chapters,
						filteredChapters: chapters
					})
				}
			);
	}

	fetchMentor() {
		fetch('/api/user', { credentials: 'include' }).then(response => response.json())
			.then((mentor) => {
				this.setState({
					username: mentor.name,
					chapter: mentor.chapter
				})
			})
	}

	fetchDashBoard(start, end, title) {

		if (!(/[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(start) || /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test(end))) {
			return;
		}

		fetch(`/api/dashboard/${start}/${end}`, { credentials: 'include' }).then(response => response.json())
			.then((dashboardArray) => {
				let dashboard = dashboardArray[0][0];
				let data = {
					allTimeWitnessingDays: (dashboard.witnessing_day != null ? dashboard.witnessing_day.toLocaleString() : "0"),
					allTimeRallies: (dashboard.rallies != null ? dashboard.rallies.toLocaleString() : "0"),
					allTimePeopleApproached: (dashboard.witnessing_day_approached != null ? dashboard.witnessing_day_approached.toLocaleString() : "0"),
					allTimeGospelPresentations: (dashboard.witnessing_day_presentations != null ? dashboard.witnessing_day_presentations.toLocaleString() : "0"),
					allTimeWitnessingIndicatedDecisions: (dashboard.witnessing_day_decisions != null ? dashboard.witnessing_day_decisions.toLocaleString() : "0"),
					allTimeRallyAttendance: (dashboard.rallies_attendance != null ? dashboard.rallies_attendance.toLocaleString() : "0"),
					allTimeRallyIndicatedDecisions: (dashboard.rally_decisions != null ? dashboard.rally_decisions.toLocaleString() : "0"),
					allTimeIndicatedDecisions: (dashboard.rally_decisions != null || dashboard.witnessing_day_decisions != null ? (dashboard.rally_decisions + dashboard.witnessing_day_decisions).toLocaleString() : "0"),
					allTimeSchoolsActive: (dashboard.schools_active != null ? dashboard.schools_active.toLocaleString() : "0"),
					allTimeSchoolsCoached: (dashboard.schools_coached != null ? dashboard.schools_coached.toLocaleString() : "0")
				}

				this.addDataCard(start, end, data, title)
			})
	}

	addDataCard(start_date, end_date, data, title) {
		let obj = {
			start: start_date,
			end: end_date,
			data: data,
			title
		}

		let newDataCards = this.state.dataCards;
		newDataCards.push(obj);

		this.setState({
			dataCards: newDataCards,
			waitingForRange: false
		})
	}

	componentDidMount() {
		this.fetchMentor();
		this.fetchSchools();
		this.fetchChapters();
		this.fetchWitnessingStories();
		this.fetchWitnessingPictures();
		this.fetchRallyStories();
		this.fetchRallyPictures();
		this.fetchDashBoard("2002-07-01", "2018-06-01", "NSP Outreach Results - All Time");
	}

	addNewRange() {
		this.setState({
			waitingForRange: true
		}, () => {
			this.fetchDashBoard(this.state.newRangeStart, this.state.newRangeEnd);
			this.modal.hide();
		})
	}

	removeCard(index) {
		let newDataCards = this.state.dataCards;
		newDataCards.splice(index, 1);
		this.setState({
			dataCards: newDataCards
		})
	}

	toggleDisplay(event) {
		this.setState({
			display: event.target.id
		})
	}

	changeStartDate(event) {
		this.setState({
			newRangeStart: event.target.value
		})
	}

	changeEndDate(event) {
		this.setState({
			newRangeEnd: event.target.value
		})
	}

	search(event) {
		this.setState({
			search: event.target.value,
			filteredSchools: _.filter(this.state.schools, (school) => {
				return school.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
			}),
			filteredChapters: _.filter(this.state.chapters, (chapter) => {
				return chapter.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
			})
		})
	}

	fetchRallyStories() {
		fetch('/api/dashboard/rallystories', { credentials: 'include' }).then(response => response.json()).then(
			(rallies) => {
				this.setState({
					storiesRallies: rallies
				})
			}
		)
	}

	fetchRallyPictures() {
		fetch('/api/dashboard/rallypictures', { credentials: 'include' }).then(response => response.json()).then(
			(rallies) => {
				this.setState({
					picturesRallies: rallies
				})
			}
		)
	}

	fetchWitnessingStories() {
		fetch('/api/dashboard/witnessingstories', { credentials: 'include' }).then(response => response.json()).then(
			storiesWitnessing => {
				this.setState({
					storiesWitnessing
				})
			}
		)
	}

	fetchWitnessingPictures() {
		fetch('/api/dashboard/witnessingpictures', { credentials: 'include' }).then(response => response.json()).then(
			picturesWitnessing => {
				this.setState({
					picturesWitnessing
				})
			}
		)
	}

	render() {
		return (
			<div>
				<div className="mdl-grid">
					<div className="mdl-cell mdl-cell--8-col mdl-cell--12-col-phone mdl-cell-12-col-tablet">
						<div className="mdl-grid">
							<div className="mdl-cell--12-col">
								<StoriesPictures
									picturesWitnessing={this.state.picturesWitnessing}
									picturesRallies={this.state.picturesRallies}
									storiesWitnessing={this.state.storiesWitnessing}
									storiesRallies={this.state.storiesRallies}
								/>
							</div>
						</div>
						<div className="mdl-grid">
							<div className="mdl-cell mdl-cell--10-col">
								<label>Display: </label>
								<label className={`${styles.radioButton} mdl-radio mdl-js-radio mdl-js-ripple-effect`} htmlFor="databox">
									<input onChange={this.toggleDisplay.bind(this)} type="radio" id="databox" className="mdl-radio__button" name="display-control" value="databox" defaultChecked />
									<span className="mdl-radio__label">Box</span>
								</label>
								<label className={`${styles.radioButton} mdl-radio mdl-js-radio mdl-js-ripple-effect`} htmlFor="datacard">
									<input onChange={this.toggleDisplay.bind(this)} type="radio" id="datacard" className="mdl-radio__button" name="display-control" value="datacard" />
									<span className="mdl-radio__label">Card</span>
								</label>
							</div>
							<div className="mdl-cell mdl-cell--2-col">
								{
									!this.state.waitingForRange ?
										<button className="mdl-button--colored mdl-button mdl-js-button mdl-button--raised" onClick={() => this.modal.show()}>
											Add Range
                    					</button>
										:
										<div className="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>
								}
							</div>
						</div>

						{
							this.state.dataCards.length > 0 ?
								<div>

									<div className={styles.resultsContainer}>
										{
											this.state.dataCards.map(
												(card, index) => {

													if (this.state.display == "databox") {
														return <DataBox
															title={card.title == null ? `${card.start} to ${card.end}` : card.title}
															data={card.data}
															changeTitle={(title) => {
																this.state.dataCards[index].title = title;
																this.setState({
																	dataCards: this.state.dataCards
																})
															}}
															onRemove={() => this.removeCard(index)}
														/>
													} else {
														return <DataCard
															title={card.title == null ? `${card.start} to ${card.end}` : card.title}
															config={this.dataCardConfig}
															obj={card.data}
															footer={this.dataCardFooter}
															changeTitle={(title) => {
																this.state.dataCards[index].title = title;
																this.setState({
																	dataCards: this.state.dataCards
																})
															}}
															onRemove={() => this.removeCard(index)}
														/>
													}
												}
											)
										}
									</div>
								</div>
								:
								""
						}

						<div className={styles.graphContainer}>
							<MainGraph ref={ref => this.graph = ref} />
						</div>
					</div>
					<div className="mdl-cell mdl-cell--4-col mdl-cell--12-col-phone mdl-cell-12-col-tablet">
					<Card useSystemCard>
						<div className="mdl-textfield mdl-js-textfield" id="searchfield">
							<input className="mdl-textfield__input" type="text" id="search" onChange={this.search.bind(this)} />
							<label className="mdl-textfield__label" htmlFor="search"><i className="material-icons">search</i> Search schools and chapters</label>
						</div>
						{
							this.state.search != "" ?
								<div>
									<hr />
									<h4>Chapters</h4>
									{
										this.state.filteredChapters.map((chapter, index) => {
											if (index < 8) {
												return <div>
													<Link
														to={{
															pathname: `/chapter/custom`,
															search: `?ids=${chapter.id}&query_title=${chapter.name}`
														}}
													>
														{chapter.name}

													</Link>
													<br />
												</div>
											} else {
												return "";
											}
										})
									}

									<hr />

									<h4>Schools</h4>

									{
										this.state.filteredSchools.map((school, index) => {
											if (index < 8) {
												return <div>
													<Link
														to={`/school/${school.id}`}
													>
														{school.name}

													</Link>
													<br />
												</div>
											} else {
												return "";
											}
										})
									}


					 			</div>
								:
								""
						}	
						</Card>
					
						<Card useSystemCard>
							<h4>Welcome{this.state.id != 3 ? ` ${this.state.username}!` : '!'}</h4>
							<p>NSP collects information about the ministry work that happens under our watch throughout each year so that we can praise God for the amazing and miraculous things that He is doing through us. Use this tool to view, analyize, and share the records that are submitted by our campus mentors and student leaders.
              				<br /><br />Click <a target="_blank" href="http://www.nationalschoolproject.com/hidden/campus-records">here</a> to submit records.
              				<br />Click <a target="_blank" href="#blank">here</a> to learn more about this tool</p>
						</Card>


					</div>
				</div>

				<SkyLight
					ref={ref => this.modal = ref}
					title="Enter Date Range"
					hideOnOverlayClicked
					dialogStyles={
						{
							top: "10%",
							marginTop: 0,
							height: "auto",
							overflow: "auto",
						}
					}>
					<div className={styles.modal}>
						<div className={styles.modalContent}>
							<DatePicker
								onChange={this.changeStartDate.bind(this)}
								value={this.state.newRangeStart}
								label="Start" />
						</div>

						<div className={styles.modalContent}>
							<DatePicker
								onChange={this.changeEndDate.bind(this)}
								value={this.state.newRangeEnd}
								label="End" />
						</div>

						<div className="">
							<button className={`mdl-button mdl-js-button mdl-button--raised ${styles.centeredButton}`} onClick={this.addNewRange.bind(this)}>
								Add
							</button>

							<button className={`mdl-button mdl-js-button mdl-button--raised ${styles.centeredButton}`} onClick={() => this.modal.hide()}>
								Cancel
							</button>
						</div>
					</div>
				</SkyLight>
			</div>
		)
	}
}
