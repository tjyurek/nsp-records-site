import React, {Component} from 'react';
import {Link} from 'react-router-dom'
import 'whatwg-fetch';
import _ from 'lodash';
import {toggleIsLoading} from '../../../utils/stateChanges';
import PageTable from '../../elements/page-table/PageTable';

import {
	getCurrentYearOfMinistry
} from '../../../utils/date_calculations';

import styles from './schools.css';

class Schools extends Component {
	constructor(props) {
		super(props);

		let current_ministry_year = getCurrentYearOfMinistry();

		this.state = {
			loading: true,
			schools: [],
			filteredSchools: [],
			viewType: "grid",
			maxDate: current_ministry_year.max_date,
			minDate: current_ministry_year.min_date,
			filterRegex:"name"
		};
	}

	handleSchoolsChange(schools) {
		this.refs.search_field.value = "";
		return this.setState({schools: schools,filteredSchools:schools});
	}

	handleLoadingStateToggle() {
		return this.setState(toggleIsLoading);
	}

	componentDidMount() {
		fetch('/api/schools',{credentials: 'include'})
			.then(response => response.json())
			.then(
				schools => {
					this.handleLoadingStateToggle();
					this.handleSchoolsChange(schools);
				}
			);
	}

	filterSchools() {
		let value = this.refs.search_field.value;
		this.setState({
			filteredSchools: _.filter(this.state.schools,(school) => {
				if (this.state.filterRegex != null && school[`${this.state.filterRegex}`] != null)
					return school[`${this.state.filterRegex}`].toString().toLowerCase().indexOf(value.toLowerCase()) !== -1;
				else
					return false;
			})
		})
	}

	handleGridClick() {
		this.setState({
			viewType:"grid",
			filterRegex:"name"
		})
	}

	handleListClick() {
		this.setState({
			viewType:"list"
		})
	}

	changeFilter(filterRegex) {
		this.refs.search_field.value = "";
		this.setState({
			filterRegex
		})
	}

	render() {
		const centerStyle = {
			textAlign:"center",
			display:"block"
		}
		return (
			<div>
				<span>
					<h2 style={centerStyle}>Schools</h2>

					<div className="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
					<label className="mdl-button mdl-js-button mdl-button--icon" htmlFor="search_field">
						<i className="material-icons">search</i>
					</label>
					<div className="mdl-textfield__expandable-holder">
						<input className="mdl-textfield__input" type="text" id="search_field" ref="search_field" onChange={this.filterSchools.bind(this)}/>
						<label className="mdl-textfield__label" htmlFor="search_field">Find a School</label>
					</div>
					</div>
					<span>
						<i className={`material-icons ${styles.iconButton} ${this.state.viewType=="grid" ? "active" : ""}`} onClick={this.handleGridClick.bind(this)}>view_module</i>
						<i className={`material-icons ${styles.iconButton} ${this.state.viewType=="list" ? "active" : ""}`} onClick={this.handleListClick.bind(this)}>view_list</i>
					</span>
				</span>

				{this.state.loading ? <div className="mdl-spinner mdl-js-spinner is-active"></div> : ''}

				{
					this.state.viewType == "grid" ?
						<ul className="mdl-grid mdl-list mdl-cell mdl-cell--12-col card-container">
							{
								this.state.filteredSchools.map(
									school => {
										return <li className="mdl-cell mdl-list__item" key={school.id}>
											<Link
												to={`/school/${school.id}`}
											>
												<button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect">
													{school.name}
												</button>
											</Link>
										</li>
									}
								)
							}
						</ul>
					: this.state.viewType == "list" ?
						<PageTable cssClass="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
							<thead className="sticky-header">
								<th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="name" ? "filtered" : ''}`} onClick={() => this.changeFilter("name")}>Name</th>
								<th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="address" ? "filtered" : ''}`} onClick={() => this.changeFilter("address")}>Address</th>
								<th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="zip" ? "filtered" : ''}`} onClick={() => this.changeFilter("zip")}>ZIP</th>
								<th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="state" ? "filtered" : ''}`} onClick={() => this.changeFilter("state")}>State</th>
								<th className={`mdl-data-table__cell--non-numeric ${this.state.filterRegex=="chapter" ? "filtered" : ''}`} onClick={() => this.changeFilter("chapter")}>Most Recent Chapter</th>
								<th></th>
							</thead>
							<tbody>
								{
									this.state.filteredSchools.map(
										(school,index) => {
											return <tr key={index}>

												<td className="mdl-data-table__cell--non-numeric">{school.name}</td>
												<td className="mdl-data-table__cell--non-numeric">{school.address}</td>
												<td className="mdl-data-table__cell--non-numeric">{school.zip}</td>
												<td className="mdl-data-table__cell--non-numeric">{school.state}</td>
												<td className={`mdl-data-table__cell--non-numeric`}><Link to={`/chapters/${school.chapter_id}`}>{school.chapter}</Link></td>
												<td>
													<Link to={`/school/${school.id}`}>
														<i className="material-icons">add</i>
													</Link>
												</td>

											</tr>
										}
									)
								}
							</tbody>
						</PageTable>
					: ''
				}
			</div>
		)
	}
}

export default Schools;
