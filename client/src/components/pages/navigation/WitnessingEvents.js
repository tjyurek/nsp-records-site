import React from 'react';
import PageTable from '../../elements/page-table/PageTable';
import Filter from '../../elements/filter/Filter';
import 'whatwg-fetch';
import {Link} from 'react-router-dom';
import moment from 'moment';

export default class WitnessingEvents extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      witnessing_days: [],
      filteredWitnessingDays: [],
      filterRegex:"name",
      showingFilter: false
    }

    this.filterColumns = [
      {
        column:"collected_data",
        display:"Collected Data",
        type:"string"
      },
      {
        column:(obj) => {
          return obj.date.substring(0,10);
        },
        display:"Date",
        type:"date"
      },
      {
        column:"chapter_name",
        display:"Chapter",
        type:"string"
      },
      {
        display:"Picture",
        column:"picture_url",
        type:"boolean"
      },
      {
        display:"Story",
        column:"stories",
        type:"string"
      },
      {
        display:"Participant Names",
        column: (obj) => {
          return obj.csp_member_names + obj.hs_student_names
        },
        type:"string"
      },
      {
        column:"location",
        display:"Location",
        type:"string"
      },
      {
        display:"Training",
        column:"training_content",
        type:"string"
      },
      {
        display:"Debrief",
        column:(obj) => {
          if (obj.was_debrief.toLowerCase().indexOf("yes") >= 0) {
            return "true"
          }
        },
        type:"boolean"
      },
      {
        display:"People Approached",
        column:"approached",
        type:"number"
      },
      {
        display:"Conversations with Christians",
        column:"conversation_with_christians",
        type:"number"
      },
      {
        column:"indicated_decisions",
        display:"Indicated Decisions",
        type:"number"
      },
      {
        display:"Recommittments",
        column:"recommittments",
        type:"number"
      },
      {
        column:"gospel_presentations",
        display:"Gospel Presentations",
        type:"number"
      },
      {
        column:"gospel_booklets",
        display:"Gospel Booklets",
        type:"nubmber"
      }
    ]
  }

  toggleFilter(event) {
    this.setState({
      showingFilter: event.target.checked
    })
  }

  fetchWitnessingDays() {
    fetch('/api/witnessing_days',{credentials: 'include'}).then(response => response.json()).then(
      witnessing_days => {
        this.setState({
          witnessing_days,
          filteredWitnessingDays: witnessing_days
        })
      }
    )
  }

  componentWillUnmount() {
    componentHandler.downgradeElements(this.root);
  }

  componentDidUpdate() {
    componentHandler.upgradeDom();
  }

  componentDidMount() {
    this.fetchWitnessingDays();
  }

  changeFilter(filterRegex) {
		this.refs.search_field.value = "";
		this.setState({
			filterRegex
		})
	}

  filterWitnessing() {
		let value = this.refs.search_field.value;
		this.setState({
			filteredWitnessingDays: _.filter(this.state.witnessing_days,(witnessing_day) => {
				if (this.state.filterRegex != null && witnessing_day[`${this.state.filterRegex}`] != null)
					return witnessing_day[`${this.state.filterRegex}`].toString().toLowerCase().indexOf(value.toLowerCase()) !== -1;
				else
					return false;
			})
		})
	}

  render() {
    return (
      <div>
        <h2 style={{"textAlign":"center"}}>Witnessing Events</h2>

        <div className="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
          <label className="mdl-button mdl-js-button mdl-button--icon" htmlFor="search_field">
            <i className="material-icons">search</i>
          </label>
          <div className="mdl-textfield__expandable-holder">
            <input className="mdl-textfield__input" type="text" id="search_field" ref="search_field" onChange={this.filterWitnessing.bind(this)} defaultChecked = {this.state.showingFilter}/>
            <label className="mdl-textfield__label" htmlFor="search_field">Find a School</label>
          </div>
        </div>
        <label className="mdl-icon-toggle mdl-js-icon-toggle mdl-js-ripple-effect" htmlFor="filter-toggle">
          <input onChange={(event) => this.toggleFilter(event)} type="checkbox" id="filter-toggle" className="mdl-icon-toggle__input"/>
          <i className="mdl-icon-toggle__label material-icons">filter_list</i>
        </label>

        <Filter
            columns= {this.filterColumns}
            showing={this.state.showingFilter}
            setState={this.setState.bind(this)}
            resultsField="filteredWitnessingDays"
            data={this.state.witnessing_days}
          />

        <PageTable cssClass="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
          <thead>
            <tr>
              <th className={`${this.state.filterRegex=="collected_data" ? "filtered" : ''}`} onClick={() => this.changeFilter("collected_data")}>Collected Data</th>
              <th className={`${this.state.filterRegex=="name" ? "filtered" : ''}`} onClick={() => this.changeFilter("name")}>School</th>
              <th className={`${this.state.filterRegex=="location" ? "filtered" : ''}`} onClick={() => this.changeFilter("location")}>Location</th>
              <th className={`${this.state.filterRegex=="date" ? "filtered" : ''}`} onClick={() => this.changeFilter("date")}>Date</th>
              <th className={`${this.state.filterRegex=="gospel_presentations" ? "filtered" : ''}`} onClick={() => this.changeFilter("gospel_presentations")}>Gospel Presentations</th>
              <th className={`${this.state.filterRegex=="indicated_decisions" ? "filtered" : ''}`} onClick={() => this.changeFilter("indicated_decisions")}>Indicated Decisions</th>
              <th className={`${this.state.filterRegex=="chapter_name" ? "filtered" : ''}`} onClick={() => this.changeFilter("chapter_name")}>Chapter Name</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.filteredWitnessingDays.map(
              (witnessing_day, index) => {
                return <tr key={index}>
                  <td>
                    {witnessing_day.collected_data}
                  </td>

                  <td>
                    <Link to={`/school/${witnessing_day.school_id}`}>
                      {witnessing_day.name}
                    </Link>
                  </td>

                  <td>
                    {witnessing_day.location}
                  </td>

                  <td>
                    {moment(witnessing_day.date.substring(0,10)).format('MMMM Do YYYY')}
                  </td>

                  <td>
                    {witnessing_day.gospel_presentations}
                  </td>

                  <td>
                    {witnessing_day.indicated_decisions}
                  </td>

                  <td>
                    <Link to={`/chapter/custom?ids=${witnessing_day.chapter_id}&min_date=${this.state.min_date}&max_date=${this.state.max_date}&query_title=${witnessing_day.chapter_name}`}>
                      {witnessing_day.chapter_name}
                    </Link>
                  </td>

                  <td>
                    <Link to={`/witnessingday/${witnessing_day.id}`}>
                      <i className="material-icons">add</i>
                    </Link>
                  </td>
                </tr>
              }
            )}
          </tbody>
        </PageTable>
        {
          this.state.filteredWitnessingDays.length > 0 ?
            <footer>
              <i>{this.state.filteredWitnessingDays.length} results</i>
            </footer>
          :
            ""
        }
      </div>
    )
  }

}
