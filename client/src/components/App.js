import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import styles from './styles.css';

class App extends Component {
	render() {
		return (
			<div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
				<header className="mdl-layout__header">
					<div className="mdl-layout__header-row">
						<span className="mdl-layout-title"><a className={styles.nspTitle} href="/">NSP Campus Records</a></span>
						<div className="mdl-layout-spacer"></div>
						<nav className="mdl-navigation mdl-layout--large-screen-only">
							<NavLink activeClassName={"mdl-navigation__link--current "+ styles.hoverEffectPermanent} className={"mdl-navigation__link "+ styles.hoverEffect} to="/chapters">Chapters</NavLink>
							<NavLink activeClassName={"mdl-navigation__link--current "+ styles.hoverEffectPermanent} className={"mdl-navigation__link "+ styles.hoverEffect} to="/schools">Schools</NavLink>
							<NavLink activeClassName={"mdl-navigation__link--current "+ styles.hoverEffectPermanent} className={"mdl-navigation__link "+ styles.hoverEffect} to="/rallies">Rallies</NavLink>
							<NavLink activeClassName={"mdl-navigation__link--current "+ styles.hoverEffectPermanent} className={"mdl-navigation__link "+ styles.hoverEffect} to="/witnessing">Witnessing</NavLink>
							<NavLink className="mdl-navigation__link" to="/">
								<button id="homebutton" className="mdl-button mdl-js-button mdl-button--icon">
									<i className="material-icons">home</i>
								</button>
								<div htmlFor="homebutton" className="mdl-tooltip">
									Go to main<br/>dashboard
								</div>
							</NavLink>
							<NavLink className="mdl-navigation__link" to="/logout">
								<button id="logoutbutton" className="mdl-button mdl-js-button mdl-button--icon">
								  <i className="material-icons">exit_to_app</i>
								</button>
								<div htmlFor="logoutbutton" className="mdl-tooltip">
									Logout
								</div>
							</NavLink>
						</nav>
					</div>
				</header>

				<div className="mdl-layout__drawer">
					<span className="mdl-layout-title">Campus Records</span>
					<nav className="mdl-navigation">
						<NavLink activeClassName="mdl-navigation__link--current" className="mdl-navigation__link" to="/chapters">Chapters</NavLink>
						<NavLink activeClassName="mdl-navigation__link--current" className="mdl-navigation__link" to="/schools">Schools</NavLink>
						<NavLink activeClassName="mdl-navigation__link--current" className="mdl-navigation__link" to="/rallies">Rallies</NavLink>
						<NavLink activeClassName="mdl-navigation__link--current" className="mdl-navigation__link" to="/witnessing">Witnessing</NavLink>
						<NavLink className="mdl-navigation__link" to="/logout">Logout</NavLink>
					</nav>
				</div>

				<main className="mdl-layout__content mdl-color--grey-100">
					<div className="page-content">
						{this.props.children}
					</div>
					<footer>
						<a href="https://www.nationalschoolproject.com">
							<img src="https://s3-us-west-2.amazonaws.com/bloomerang-public-cdn/californiaschoolproject/.secureWidgetHosting/orgLogo.png?nocache=636457817481952000"/>
						</a>
						<br/>
						Questions? Contact <a href="mailto:it@nationalschoolproject.com">it@nationalschoolproject.com</a>
					</footer>
				</main>
			</div>
		)
	}
}

export default App;
