import React, {Component} from 'react';
import styles from './styles.css';

/**
 * @description A white card to put things on
 * @prop {boolean} useSystemCard If true, the card will not be displayed with MDL styles but rather records site styling
 */
class Card extends Component {

	render() {
		const { cssClass, useSystemCard } = this.props;

		return (
			<div className={`${useSystemCard ? styles.systemCard : 'mdl-color--white mdl-shadow--2dp'} ${cssClass} ${styles.card}`}>
				{this.props.children}
			</div>
		)
	}
}

export default Card;
