import React from 'react';
import moment from 'moment';
import styles from './styles.css';

/**
 * @description This component is the stories card on the main dashboard
 * @todo Make this component more resuable
 */
export default class StoriesPictures extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			index: 0,
			boxes: 3
		}
	}

	left() {
		if (this.state.index > 0)
			this.setState({
				index: this.state.index - 1
			})
	}

	right(targetArray) {
		if (this.state.index < targetArray.length)
			this.setState({
				index: this.state.index + 1
			})
	}

	resize() {
		if (window.innerWidth < 1296 && window.innerWidth > 612) {
			this.setState({
				index: this.state.boxes == 2 ? this.state.index : 0,
				boxes: 2
			})
		} else if (window.innerWidth < 612) {
			this.setState({
				index: this.state.boxes == 1 ? this.state.index : 0,
				boxes: 1
			})
		} else {
			this.setState({
				index: this.state.boxes == 1 ? this.state.index : 0,
				boxes: 3
			})
		}
	}

	componentDidMount() {
		this.resize();
		window.addEventListener("resize", this.resize.bind(this));
	}

	componentWillUnmount() {
		window.removeEventListener("resize", this.resize.bind(this));
	}

	render() {
		return (
			<div className={styles.storiesBox}>
				<h4 className={styles.storiesBoxTitle}>Stories</h4>

				<div className={styles.storiesButtonContainer}>
					<div className={styles.storiesButtonTable}>
						<div className={styles.storiesButtonTableCell}>
							<button onClick={() => this.left(this.props.storiesWitnessing)} className="mdl-button mdl-js-button mdl-button--icon">
								<i className="material-icons">arrow_back</i>
							</button>
						</div>
					</div>
				</div>

				{
					this.props.storiesWitnessing.map((obj, index) => {
						if (index >= (this.state.index * this.state.boxes) && index < ((this.state.index + 1) * this.state.boxes)) {
							return (
								<div className={styles.story} onClick={() => { window.location.href = `/witnessingday/${obj.id}` }}>
									<div className={styles.storyContent}>
										{obj.data.length > 220 ? obj.data.substring(0, 220) + "..." : obj.data}
									</div>

									<div className={styles.storyAuthor}>
										<p className={styles.storiesLink}>-{obj.collected_data}</p>
										<p className={styles.smallFont}>Witnessing - <a href={`/school/${obj.school_id}`}>{obj.school_name.substring(0, 5)}...</a> {moment(obj.date).format("MM/DD/YY")}</p>
									</div>
								</div>
							)
						}
					})
				}

				<div className={styles.storiesButtonContainer}>
					<div className={styles.storiesButtonTable}>
						<div className={styles.storiesButtonTableCell}>
							<button onClick={() => this.right(this.props.storiesWitnessing)} className="mdl-button mdl-js-button mdl-button--icon">
								<i className="material-icons">arrow_forward</i>
							</button>
						</div>
					</div>
				</div>

			</div>
		)
	}
}
