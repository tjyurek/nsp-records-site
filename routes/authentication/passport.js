import bcrypt from 'bcrypt-nodejs';
import authConfig from './auth';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import { getSiteUrl, isStudentDeveloper } from '../util';

let google = require('googleapis');
let privatekey = require('./creds.json');
let LocalStrategy = require('passport-local').Strategy;

function getPermissions(user, callback) {

	if (isStudentDeveloper(user)) {
		return callback({
			has_access: true,
			has_full_access: true,
			schools: [],
			chapters: [],
			admin: false
		}, "Student Developer", user);
	}

	let jwtClient = new google.auth.JWT(
		privatekey.client_email,
		null,
		privatekey.private_key,
		['https://www.googleapis.com/auth/admin.directory.user'],
		"joseph.s@nationalschoolproject.com");

	jwtClient.authorize((err, tokens) => {
		if (err) {
			console.log("Server failed to authenticate google service account");
			console.log(err);
			return callback(null);
		}

		var service = google.admin('directory_v1');
		service.users.get({
			auth: jwtClient,
			userKey: user,
			projection: "full"
		}, function (err, response) {
			if (err) {
				console.log('The API returned an error: ' + err);
				return callback(null);
			}
			try {
				return callback(response.customSchemas.Records, response.name.fullName, user);
			} catch (err) {
				console.log(err);
				return callback(null);
			}
		});
	})
}

export default function initPassport(passport, pool) {
	passport.serializeUser((user, done) => {

		if (process.env.CLOG) {
			console.log("USER TOKEN: " + user.token)
			console.log("USER REFRESH TOKEN: " + user.refreshToken);
		}

		if (user.schools == undefined) {
			user.schools = [];
		}

		if (user.chapters == undefined) {
			user.chapters = [];
		}

		done(null, {
			name: user.name, chapter: user.chapter, email: user.email, mentorId: user.mentorId, hasFullAccess: user.hasFullAccess, isAdmin: user.isAdmin, hasAccess: user.hasAccess, schools: user.schools.map(
				(obj) => {
					let val = parseInt(obj.value);
					if (!isNaN(val)) {
						return val
					} else {
						return -1;
					}
				}
			), chapters: user.chapters.map(
				(obj) => {
					let val = parseInt(obj.value);
					if (!isNaN(val)) {
						return val
					} else {
						return -1;
					}
				}
			), token: user.token, refreshToken: user.refreshToken
		});
	})

	passport.deserializeUser((user, done) => {
		done(null, user);
	})

	passport.use('google', new GoogleStrategy({
		clientID: authConfig.googleAuth.clientID,
		clientSecret: authConfig.googleAuth.clientSecret,
		callbackURL: `${getSiteUrl()}/googlecallback`,
	},
		(token, refreshToken, profile, done) => {
			process.nextTick(() => {

				let found = false;

				for (var i = 0; i < profile.emails.length; i++) {
					if (/.+@nationalschoolproject\.com/g.test(profile.emails[i].value) || isStudentDeveloper(profile.emails[i].value)) {
						found = true;
						getPermissions(profile.emails[i].value, (data, displayName, email) => {
							if (data == null) {
								return done(null, false);
							} else {

								if (data.has_access || data.has_full_access) {
									return done(null, {
										name: displayName,
										email: email,
										chapter: "NA",
										mentorId: 3,
										hasFullAccess: data.has_full_access,
										hasAccess: data.has_access,
										schools: data.schools,
										chapters: data.chapters,
										isAdmin: data.admin,
										token: token,
										refreshToken: refreshToken
									})
								} else {
									return done(null, false)
								}
							}
						});
					}
				}

				if (!found)
					return done(null, false);
			})
		}
	))

	passport.use('local-auth', new LocalStrategy({
		usernameField: "email",
		passwordField: "password",
		passReqToCallback: true
	},

		(req, email, password, done) => {

			pool.getConnection((err, connection) => {

				connection.query(`SELECT *,chapters.name AS 'chapter_name' FROM records_users
          JOIN mentors ON records_users.mentor_id = mentors.id
          JOIN chapters ON mentors.chapter=chapters.id
          WHERE mentors.email='${email}'`, (err, results) => {

						if (err || results[0] == undefined) {
							console.log(err);
							return done(null, false);
						};
						if (bcrypt.compareSync(password, results[0].passhash)) {
							return done(null, {
								name: `${results[0].first_name} ${results[0].last_name}`,
								chapter: results[0].chapter_name,
								email: results[0].email,
								mentorId: results[0].mentor_id,
								hasFullAccess: true,
								hasAccess: true,
								schools: [],
								chapters: [],
							})
						} else {
							return done(null, false);
						}

					});
			});

		}

	))
}
