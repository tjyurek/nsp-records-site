import { withDBConnection, isLoggedIn } from '../util';
import {
	getAllRaillesByChapterIds,
	getRallies,
	getRalliesByChapterId,
	getRally,
	getRalliesBySchoolId,
	getLiteRalliesBySchoolId
} from './RalliesController';

export default app => {
	app.route('/api/rallies')
		.get(isLoggedIn, withDBConnection(getRallies));

	app.route('/api/rallies/:id')
		.get(isLoggedIn, withDBConnection(getRally));

	app.route('/api/rallies/chapter_id/:id/:start_date/:end_date')
		.get(isLoggedIn, withDBConnection(getRalliesByChapterId));

	app.route('/api/rallies/custom/chapter_ids/:ids')
		.get(isLoggedIn, withDBConnection(getAllRaillesByChapterIds));

	app.route('/api/rallies/school_id/:id')
		.get(isLoggedIn, withDBConnection(getRalliesBySchoolId));

	app.route('/api/rallies/lite/school_id/:id')
		.get(isLoggedIn, withDBConnection(getLiteRalliesBySchoolId));
}
