import parseQuery from './QueryParser';

// Use this map to select a join statement for a given entity/groupByEntity combination
// Used like joinStatementMap[entity][groupByEntity]
const joinStatementMap = {
	rallies: {
		chapters: 'JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id JOIN chapters ON school_registration.chapter_id = chapters.id',
		schools: 'JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id JOIN schools ON school_registration.school_id = schools.id',
		year: ''
	},
	witnessing_day: {
		chapters: 'JOIN chapters ON witnessing_day.chapter_id = chapters.id',
		schools: 'JOIN school_registration ON rallies.schoolreg_id = school_registration.registration_id JOIN schools ON school_registration.school_id = schools.id',
		year: ''
	},
	schools: {
		chapters: 'JOIN chapters ON school_registration.chapter_id = chapters.id',
	}
}

const fromMap = {
	schools: 'school_registration JOIN schools ON schools.id = school_registration.school_id',
}

const groupByTokens = {
	rallies: {
		chapters: 'chapters.name',
		schools: 'schools.name',
		year: `IF(
			MONTH(date) <= 6,
			CONCAT(YEAR(date)-1, "-", YEAR(date)),
			CONCAT(YEAR(date),"-",YEAR(date)+1)
		)`
	},
	witnessing_day: {
		chapters: 'chapters.name',
		schools: 'schools.name',
		year: `IF(
			MONTH(date) <= 6,
			CONCAT(YEAR(date)-1, "-", YEAR(date)),
			CONCAT(YEAR(date),"-",YEAR(date)+1)
		)`
	},
	schools: {
		chapters: 'chapters.name',
		year: `IF(
			MONTH(start_date) <= 6,
			CONCAT(YEAR(start_date)-1, "-", YEAR(start_date)),
			CONCAT(YEAR(start_date),"-",YEAR(start_date)+1)
		)`
	}
}

export const fieldAnomalies = {
	'schools.students_at_school': {
		SELECT: '(SELECT AVG(school_registration.students_at_school) FROM school_registration WHERE school_registration.school_id = schools.id AND students_at_school > 0)',
	},
	'rallies.was_contact_cards': {
		SELECT: 'rallies.was_contact_cards LIKE "YES"',
	},
	'rallies.materials': {
		SELECT: 'rallies.materials_bibles + rallies.materials_gospel_booklets + rallies.materials_gospels_of_john + rallies.materials_lifebooks + rallies.materials_other',
		WHERE: 'rallies.materials_bibles > 0 OR rallies.materials_gospel_booklets > 0 OR rallies.materials_gospels_of_john > 0 OR rallies.materials_lifebooks > 0 OR rallies.materials_other > 0'
	},
	'rallies.materials_bibles': {
		WHERE: 'rallies.materials_bibles > 0',
	},
	'rallies.materials_gospel_booklets': {
		WHERE: 'rallies.materials_gospel_booklets > 0'
	},
	'rallies.materials_gospels_of_john': {
		WHERE: 'rallies.materials_gospels_of_john > 0',
	},
	'rallies.materials_lifebooks': {
		WHERE: 'rallies.materials_lifebooks > 0',
	},
	'rallies.materials_other': {
		WHERE: 'rallies.materials_other > 0',
	},
	'rallies.was_materials_table': {
		SELECT: 'rallies.was_materials_table LIKE "YES"'
	},
	'rallies.was_performer': {
		SELECT: 'rallies.was_performer LIKE "YES"',
	},
	'rallies.was_speaker': {
		SELECT: 'rallies.was_speaker LIKE "YES"',
	},
	'rallies.speaker_explained_saved': {
		SELECT: 'rallies.speaker_explained_saved LIKE "YES"'
	},
	'rallies.speaker_gospel_shared': {
		SELECT: 'rallies.speaker_gospel_shared LIKE "YES"'
	},
	'rallies.speaker_physical_invitation': {
		SELECT: 'rallies.speaker_physical_invitation LIKE "YES"'
	},
	'rallies.speaker_student': {
		SELECT: 'rallies.speaker_student_guest LIKE "student"'
	},
	'rallies.speaker_guest': {
		SELECT: 'rallies.speaker_student_guest LIKE "guest"'
	},
	'rallies.speaker_topically_known': {
		SELECT: 'rallies.speaker_topically_known LIKE "YES"'
	},
	'rallies.chapter_id': {
		SELECT: '(SELECT school_registration.chapter_id FROM school_registration WHERE registration_id = schoolreg_id)',
	},
	'rallies.school_id': {
		SELECT: '(SELECT school_registration.school_id FROM school_registration WHERE registration_id = schoolreg_id)',
	},
	'witnessing_day.was_location_hs': {
		SELECT: 'witnessing_day.was_location_hs LIKE "YES"'
	},
	'witnessing_day.school_id': {
		SELECT: '(SELECT school_registration.school_id FROM school_registration WHERE registration_id = schoolreg_id)',
	},
}

const methods = [
	'avg',
	'count',
	'sum',
	'max',
	'min',
]

const getField = (field, entity) => {
	if (entity === 'schools' && !field) {
		return 'DISTINCT school_registration.school_id'
	}
	return ((fieldAnomalies[field] && fieldAnomalies[field].SELECT) || field || '*');
}

const getJoin = (entity, groupByEntity) => {
	if (!groupByEntity)
		return '';
	return joinStatementMap[entity][groupByEntity] || '';
}

const hasWhereClause = queryObject => {
	return (queryObject.qualifier || queryObject.groupByEntityQualifier || (fieldAnomalies[queryObject.field] && fieldAnomalies[queryObject.field].WHERE))
}

const isValidMethod = method => {
	for (let test in methods) {
		if (method.toLowerCase() === methods[test])
		 return true;
	}
	return false;
}

export const buildSQL = (method, query) => {
	if (typeof method !== 'string' || !isValidMethod(method))
		return {err: 'BadMethod', reason: `${method} is not a valid method`};

	const queryObject = parseQuery(query);

	if (queryObject.err) {
		return queryObject;
	}

	return `
		SELECT ${method.toUpperCase()}(${getField(queryObject.field, queryObject.entity)}) AS result
		${queryObject.groupByEntity ? `, ${groupByTokens[queryObject.entity][queryObject.groupByEntity]} AS name` : ''}
		FROM ${fromMap[queryObject.entity] || queryObject.entity}
		${getJoin(queryObject.entity, queryObject.groupByEntity)}
		${
			!hasWhereClause(queryObject) ?
			'' :

			`
				WHERE
				${queryObject.qualifier ? `(${queryObject.qualifier}) ` : ''}
				${queryObject.groupByEntityQualifier ? `${queryObject.qualifier ? ' AND ' : ''}(${queryObject.groupByEntityQualifier})` : ''}
				${(fieldAnomalies[queryObject.field] && fieldAnomalies[queryObject.field].WHERE) ? `${queryObject.qualifier || queryObject.groupByEntityQualifier ? ' AND ' : ''}(${fieldAnomalies[queryObject.field].WHERE})` : ''}
			`
		}
		${
			queryObject.groupByEntity ?
			`GROUP BY ${groupByTokens[queryObject.entity][queryObject.groupByEntity]}`
			: ''
		}
	`
}