import {
	withDBConnection,
	isLoggedIn,
} from '../util';
import { Analyize } from './AnalyticsController';

export default app => {
	app.route('/analytics/:method')
		.get(isLoggedIn, withDBConnection(Analyize));
}
