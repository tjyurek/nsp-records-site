import PDFDocument from 'pdfkit';
import PDFTable from 'voilab-pdf-table'
import assets from './assets'

export default function downloadDashboard(request, response) {
	response.setHeader('Content-Type', 'application/pdf');
	response.setHeader('Content-Disposition', 'attachment; filename=chapter-report.pdf');

	let body = request.body;

	let pdf = new PDFDocument()
	pdf.pipe(response);

	let table = new PDFTable(pdf, {
		bottomMargin: 30
	})
	let dataCards = body.dataCards;

	pdf.image("./nsp-metrics/assets/nsplogo.png", { width: 468 }).moveDown()

	pdf.fontSize(28).font("./nsp-metrics/assets/Abel-Regular.ttf")
		.text("Records Report", assets.title)
		.moveDown()
		.fontSize(12)

	pdf.image(body.graph, { width: 468 })

	pdf.text("hello there!");

	pdf.end()
}
