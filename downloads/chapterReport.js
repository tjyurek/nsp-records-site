import PDFDocument from 'pdfkit';
import PDFTable from 'voilab-pdf-table';
import _ from 'lodash';

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function p(numerator, denominator) {
	return ((numerator / denomiator) * 100).toFixed(2) + "%";
}

const downloadChapter = (request, response, connection) => {
	//const {ids} = request.params;

	let schoolInfo = [];
	let witnessingInfo = [];
	let rallyInfo = [];

	/*
	connection.query('SELECT SCHOOLS STATEMENT',
	  (error,results1,fields) => {
		if (!error) {
		  schoolInfo = results1;
		  connection.query('SELECT WITNESSING STATEMENT',
		  (error,results2,fields) => {
			if (!error) {
			  witnessingInfo = results2;
			  connection.query('SELECT RALLIES STATEMENT',
			  (error, results3, fields) => {
				if (!error) {
				  connection.release();
				  rallyInfo = results3;
				  */
	genPdf(response, schoolInfo, witnessingInfo, rallyInfo);
	/*
  }
});
}
});
}
});
*/
}

const genPdf = (response, schoolInfo, witnessingInfo, rallyInfo) => {
	const nspyellow = "#EEA00F";
	const nspblue = "#1A2D6B";
	const black = "black";
	const cont = { continued: true };
	const title = { underline: true, align: "center" };
	const indent1Cont = { indent: 36, continued: true };
	const indent2Cont = { indent: 72, continued: true };


	/*
	  DEFINING REPORT DATA HERE
	*/
	let allSchools = [];
	for (var i = 0; i < 50; i++) {
		allSchools[i] = {
			name: `School ${i}`,
			rallies: getRandomInt(0, 3),
			witnessing: getRandomInt(0, 5),
		};
	}

	let chapter = "Biola";

	/*
	  END VAR DEFINITIONS
	*/

	response.setHeader('Content-Type', 'application/pdf');
	response.setHeader('Content-Disposition', 'attachment; filename=chapter-report.pdf');

	const pdf = new PDFDocument();
	pdf.pipe(response);

	//===TITLE PAGE===
	pdf.fontSize(28);
	pdf.image("./nsp-metrics/assets/nsplogo.png", { width: 468 });
	pdf.font("./nsp-metrics/assets/Abel-Regular.ttf").moveDown().text("NSP Metrics Report")
	pdf.fontSize(12).text("For range 7/1/16 to 6/31/17");
	pdf.fontSize(28).fillColor(nspblue).text(chapter, 100, 324, title).fillColor(black);

	/*
	pdf.addPage();
	//===GENERAL INFORMATION===
	pdf.fontSize(28).text("GENERAL INFORMATION",title);
	pdf.fontSize(12).text("All data within range", {align:"center",features:["ital"]});
	pdf.moveDown().fontSize(16).text("Enrolled Schools: ",cont).fillColor(nspyellow).text(allSchools.length).fillColor(black);
	pdf.text("Total Campus Mentors: ",cont).fillColor(nspyellow).text("25").fillColor(black);
	pdf.text("Total Gospel Presentations: ",cont).fillColor(nspyellow).text("300").fillColor(black);
	pdf.text("Total Materials Distributed: ",cont).fillColor(nspyellow).text("200").fillColor(black);
	pdf.text("Indicated Decisions: ",cont).fillColor(nspyellow).text("1,232").fillColor(black);
  
	pdf.moveDown().text("Outreach Events: ", cont).fillColor(nspyellow).text("400").fillColor(black);
	pdf.text("Rallies: ",indent1Cont).fillColor(nspyellow).text("200").fillColor(black);
	pdf.text("Total Rally Attendance: ",indent2Cont).fillColor(nspyellow).text("13,600").fillColor(black);
	pdf.text("Total Gospel Presentations: ",indent2Cont).fillColor(nspyellow).text("100 (50%)").fillColor(black);
	pdf.text("Total First Time Decisions/Recommitments: ",indent2Cont).fillColor(nspyellow).text("1,100").fillColor(black);
	pdf.text("Total Materials Distributed: ",indent2Cont).fillColor(nspyellow).text("100").fillColor(black);
	pdf.moveDown().text("Witnessing events: ",indent1Cont).fillColor(nspyellow).text("200").fillColor(black);
	pdf.text("Total People Approached: ",indent2Cont).fillColor(nspyellow).text("10,000").fillColor(black);
	pdf.text("Total Gospel Presentations: ",indent2Cont).fillColor(nspyellow).text("5,000 (50%)").fillColor(black);
	pdf.text("Total First Time Decisions/Recommitments: ",indent2Cont).fillColor(nspyellow).text("89").fillColor(black);
	pdf.text("Total Materials Distributed: ",indent2Cont).fillColor(nspyellow).text("100").fillColor(black);
  
	/*
	pdf.addPage();
	//===SCHOOL INFORMATION===
	let all = "";
	let schoolsNoWitnessing = "";
	let schoolsNoWitnessingCount = 0;
	let schoolsNoRallies = "";
	let schoolsNoRalliesCount = 0;
	let avgRallyPerSchool = 0;
	let avgWitnessingPerSchool = 0;
	let avgOutreachEventsPerSchool = 0;
	let events0 = 0;
	let events0Names = "";
	let events1 = 0;
	let events1Names = "";
	let events2_3 = 0;
	let events2_3Names = "";
	let events4_6 = 0;
	let events4_6Names = "";
	let events7 = 0;
	let events7Names = "";
	for (var i = 1; i < allSchools.length; i++) {
	  let r = allSchools[i].rallies;
	  let w = allSchools[i].witnessing;
	  let t = r+w;
	  all = all + ((i != 0) ? ",   " : "") + allSchools[i].name;
	  avgRallyPerSchool += r;
	  avgWitnessingPerSchool += w;
  
	  if (w == 0) {
		schoolsNoWitnessing = schoolsNoWitnessing + ((schoolsNoWitnessingCount != 0) ? ",   " : "") + allSchools[i].name;
		schoolsNoWitnessingCount++;
	  }
  
	  if (r == 0) {
		schoolsNoRallies = schoolsNoRallies + ((schoolsNoRalliesCount != 0) ? ",   " : "") + allSchools[i].name;
		schoolsNoRalliesCount++;
	  }
  
	  if (t == 0) {
		events0Names = events0Names + ((events0 != 0) ? ",   " : "") + allSchools[i].name;
		events0 ++;
	  } else if (t == 1) {
		events1Names = events1Names + ((events1 != 0) ? ",   " : "") + allSchools[i].name;
		events1 ++;
	  } else if (t > 1 && t <= 3) {
		events2_3Names = events2_3Names + ((events2_3 != 0) ? ",   " : "") + allSchools[i].name;
		events2_3 ++;
	  } else if (t > 3 && t <= 6) {
		events4_6Names = events4_6Names + ((events4_6 != 0) ? ",   " : "") + allSchools[i].name;
		events4_6 ++;
	  } else {
		events7Names = events7Names + ((events7 != 0) ? ",   " : "") + allSchools[i].name;
		events7  ++;
	  }
	}
	avgRallyPerSchool = (avgRallyPerSchool/allSchools.length).toFixed(2);
	avgWitnessingPerSchool = (avgWitnessingPerSchool/allSchools.length).toFixed(2);
  
	pdf.fontSize(28).text("SCHOOL INFORMATION",title);
	pdf.fontSize(12).text("All data within range",{align:"center"});
	pdf.fontSize(16).moveDown().text("All Schools");
	pdf.fontSize(10).text(all).fontSize(16);
  
	pdf.moveDown(2).text("Average Rallies Per School: ",cont).fillColor(nspyellow).text(avgRallyPerSchool).fillColor(black);
	pdf.text("Average Witnessing Events Per School: ",cont).fillColor(nspyellow).text(avgWitnessingPerSchool).fillColor(black);
  
	pdf.moveDown(2).text("Schools with no Witnessing: ",cont).fillColor(nspyellow).text(schoolsNoWitnessingCount).fillColor(black);
	pdf.fontSize(10).text(schoolsNoWitnessing).fontSize(16);
  
	pdf.moveDown().text("Schools with no Rallies: ",cont).fillColor(nspyellow).text(schoolsNoRalliesCount).fillColor(black);
	pdf.fontSize(10).text(schoolsNoRallies).fontSize(16);
  
	pdf.moveDown(2).text("Schools with No Events: ",cont).fillColor(nspyellow).text(events0).fillColor(black);
	pdf.fontSize(10).text(events0Names).fontSize(16);
  
	pdf.moveDown().text("Schools with 1 Event: ",cont).fillColor(nspyellow).text(events1).fillColor(black);
	pdf.fontSize(10).text(events1Names).fontSize(16);
  
	pdf.moveDown().text("Schools with 2-3 Events: ",cont).fillColor(nspyellow).text(events2_3).fillColor(black);
	pdf.fontSize(10).text(events2_3Names).fontSize(16);
  
	pdf.moveDown().text("Schools with 4-6 Events: ",cont).fillColor(nspyellow).text(events4_6).fillColor(black);
	pdf.fontSize(10).text(events4_6Names).fontSize(16);
  
	pdf.moveDown().text("Schools with 7+ Events: ",cont).fillColor(nspyellow).text(events7).fillColor(black);
	pdf.fontSize(10).text(events7Names).fontSize(16);
  
	pdf.addPage();
	//===WITNESSING INFORMATION===
	pdf.fontSize(28).text("WITNESSING INFORMATION",title);
	pdf.fontSize(12).text("All data within range",{align:"center"});
	pdf.fontSize(16).moveDown().text("Total People Approached: ",cont).fillColor(nspyellow).text("10,000").fillColor(black);
	pdf.text("Converstaions with Christians: ",cont).fillColor(nspyellow).text("4,000");
	pdf.text("Satisfied Presentations", indent1Cont).fillColor(nspyellow).text("1,200");
	*/

	pdf.end();
}

export { downloadChapter };
